# Description:
#   Showing of redmine issue via the REST API
#   It also listens for the #nnnn format and provides issue data and link
#   Eg. "Hey guys check out #273"
#
# Dependencies:
#   None
#
# Configuration:
#   HUBOT_REDMINE_SSL
#   HUBOT_REDMINE_BASE_URL
#   HUBOT_REDMINE_TOKEN
#   HUBOT_REDMINE_IGNORED_USERS
#
# Commands:
#   hubot (redmine|show) me <issue-id> - Show the issue status
#   hubot show (my|user's) issues - Show your issues or another user's issues
#   hubot assign <issue-id> to <user-first-name> ["notes"] - Assign the issue to the user (searches login or firstname)
#   hubot update <issue-id> with "<note>" - Adds a note to the issue
#   hubot add <hours> hours to <issue-id> ["comments"] - Adds hours to the issue with the optional comments
#   hubot link me <issue-id> - Returns a link to the redmine issue
#   hubot set <issue-id> to <int>% ["comments"] - Updates an issue and sets the percent done
#
# URLS:
#   /hubot/bitbucket-commit
#   /hubot/beanstalk-svn
#   /hubot/beanstalk-commit
#
# Notes:
#   <issue-id> can be formatted in the following ways: 1234, #1234,
#   issue 1234, issue #1234
#
# Author:
#   robhurring

if process.env.HUBOT_REDMINE_SSL?
  HTTP = require('https')
else
  HTTP = require('http')

URL = require('url')
QUERY = require('querystring')

base_url = process.env.HUBOT_REDMINE_BASE_URL
base_token = process.env.HUBOT_REDMINE_TOKEN

# base_url = 'http://tyredmine.herokuapp.com'
# base_token = 'e30d12b9f7f086149d44f32cd487560d377a0bcb'

status_list =
  1: 'New'
  2: 'Assigned'
  3: 'Resolved'
  4: 'Requesting Feedback'
  5: 'Verified (close)'
  6: 'Rejected (close)'
  8: 'Resolved - on prod'
  9: 'Resolved - on dev stage'
  11: 'Approved - push to prod!'
  12: 'Verified - on dev stage'

module.exports = (robot) ->
  redmine = new Redmine base_url, base_token

  # Robot link me <issue>
  robot.respond /link me (?:issue )?(?:#)?(\d+)/i, (msg) ->
    id = msg.match[1]
    msg.reply "#{redmine.url}/issues/#{id}"

  # Robot set <issue> to <percent>% ["comments"]
  robot.respond /set (?:issue )?(?:#)?(\d+) to (\d{1,3})%?(?: "?([^"]+)"?)?/i, (msg) ->
    [id, percent, notes] = msg.match[1..3]
    percent = parseInt percent

    if notes?
      notes = "#{msg.message.user.name}: #{userComments}"
    else
      notes = "Ratio set by: #{msg.message.user.name}"

    attributes =
      "notes": notes
      "done_ratio": percent

    redmine.Issue(id).update attributes, (err, data, status) ->
      if status == 200
        msg.reply "Set ##{id} to #{percent}%"
      else
        msg.reply "Update failed! (#{err})"

  # Robot add <hours> hours to <issue_id> ["comments for the time tracking"]
  robot.respond /add (\d{1,2}) hours? to (?:issue )?(?:#)?(\d+)(?: "?([^"]+)"?)?/i, (msg) ->
    [hours, id, userComments] = msg.match[1..3]
    hours = parseInt hours

    if userComments?
      comments = "#{msg.message.user.name}: #{userComments}"
    else
      comments = "Time logged by: #{msg.message.user.name}"

    attributes =
      "issue_id": id
      "hours": hours
      "comments": comments

    redmine.TimeEntry(null).create attributes, (error, data, status) ->
      if status == 201
        msg.reply "Your time was logged"
      else
        msg.reply "Nothing could be logged. Make sure RedMine has a default activity set for time tracking. (Settings -> Enumerations -> Activities)"

  # Robot show <my|user's> [redmine] issues
  robot.respond /show (?:my|(\w+\'s)) (?:redmine )?issues/i, (msg) ->
    userMode = true
    firstName =
      if msg.match[1]?
        userMode = false
        msg.match[1].replace(/\'.+/, '')
      else
        msg.message.user.name.split(/\s/)[0]

    redmine.Users name:firstName, (err,data) ->
      unless data.total_count > 0
        msg.reply "Couldn't find any users with the name \"#{firstName}\""
        return false

      user = resolveUsers(firstName, data.users)[0]

      params =
        "assigned_to_id": user.id
        "limit": 25,
        "status_id": "open"
        "sort": "priority:desc",

      redmine.Issues params, (err, data) ->
        if err?
          msg.reply "Couldn't get a list of issues for you!"
        else
          _ = []

          if userMode
            _.push "You have #{data.total_count} issue(s)."
          else
            _.push "#{user.firstname} has #{data.total_count} issue(s)."

          for issue in data.issues
            do (issue) ->
              _.push "\n[#{issue.tracker.name} - #{issue.priority.name} - #{issue.status.name}] ##{issue.id}: #{issue.subject}"

          msg.reply _.join "\n"

  # Robot update <issue> with "<note>"
  robot.respond /update (?:issue )?(?:#)?(\d+)(?:\s*with\s*)?(?:[-:,])? (?:"?([^"]+)"?)/i, (msg) ->
    [id, note] = msg.match[1..2]

    attributes =
      "notes": "#{msg.message.user.name}: #{note}"

    redmine.Issue(id).update attributes, (err, data, status) ->
      unless data?
        if status == 404
          msg.reply "Issue ##{id} doesn't exist."
        else
          msg.reply "Couldn't update this issue, sorry :("
      else
        msg.reply "Done! Updated ##{id} with \"#{note}\""

  # Robot add issue to "<project>" [traker <id>] with "<subject>"
  robot.respond /add (?:issue )?(?:\s*to\s*)?(?:"?([^" ]+)"? )(?:tracker\s)?(\d+)?(?:\s*with\s*)("?([^"]+)"?)/i, (msg) ->
    [project_id, tracker_id, subject] = msg.match[1..3]

    attributes =
      "project_id": "#{project_id}"
      "subject": "#{subject}"

    if tracker_id?
      attributes =
        "project_id": "#{project_id}"
        "subject": "#{subject}"
        "tracker_id": "#{tracker_id}"

    redmine.Issue().add attributes, (err, data, status) ->
      unless data?
        if status == 404
          msg.reply "Couldn't update this issue, #{status} :("
      else
        msg.reply "Done! Added issue #{data.id} with \"#{subject}\""

  # Robot assign <issue> to <user> ["note to add with the assignment]
  robot.respond /assign (?:issue )?(?:#)?(\d+) to (\w+)(?: "?([^"]+)"?)?/i, (msg) ->
    [id, userName, note] = msg.match[1..3]

    redmine.Users name:userName, (err, data) ->
      unless data.total_count > 0
        msg.reply "Couldn't find any users with the name \"#{userName}\""
        return false

      # try to resolve the user using login/firstname -- take the first result (hacky)
      user = resolveUsers(userName, data.users)[0]

      attributes =
        "assigned_to_id": user.id

      # allow an optional note with the re-assign
      attributes["notes"] = "#{msg.message.user.name}: #{note}" if note?

      # get our issue
      redmine.Issue(id).update attributes, (err, data, status) ->
        unless data?
          if status == 404
            msg.reply "Issue ##{id} doesn't exist."
          else
            msg.reply "There was an error assigning this issue."
        else
          msg.reply "Assigned ##{id} to #{user.firstname}."
          msg.send '/play trombone' if parseInt(id) == 3631

  # Robot redmine me <issue>
  robot.respond /(?:redmine|show)(?: me)? (?:issue )?(?:#)?(\d+)/i, (msg) ->
    id = msg.match[1]

    params =
      "include": "journals"

    redmine.Issue(id).show params, (err, data, status) ->
      unless status == 200
        msg.reply "Issue ##{id} doesn't exist."
        return false

      url = "#{redmine.url}/issues/#{id}"

      robot.emit 'slack-attachment', formatIssueMessage data, url, msg

  # Listens to #NNNN and gives ticket info
  robot.hear /.*(#(\d+)).*/, (msg) ->
    id = msg.match[1].replace /#/, ""

    ignoredUsers = process.env.HUBOT_REDMINE_IGNORED_USERS or ""

    #Ignore cetain users, like Redmine plugins
    if msg.message.user.name in ignoredUsers.split(',')
      return

    if isNaN(id)
      return

    params =
      "include": "journals"

    redmine.Issue(id).show params, (err, data, status) ->
      unless status == 200
        # Issue not found, don't say anything
        return false

      url = "#{redmine.url}/issues/#{id}"

      robot.emit 'slack-attachment', formatIssueMessage data, url, msg

  # For Bitbucket git hooks
  robot.router.post '/hubot/bitbucket-commit', (req, res) ->
    data = JSON.parse req.body.payload

    for commit in data.commits
      if commit.message.indexOf("refs #") > -1
        haystack = /.*(#(\d+)).*/.exec commit.message
        issue_id = haystack[2]

        author = /(.*) <(.*)>/.exec commit.raw_author

        attributes =
          "notes": "#{author[1]} changed #{commit.files.length} files on #{commit.branch} branch:\n#{commit.message}\n#{data.canon_url}#{data.repository.absolute_url}commits/#{commit.node}"

        redmine.Issue(issue_id).update attributes, (isserr, issdata, issstatus) ->


    res.writeHead 204, { 'Content-Length': 0 }
    res.end()

  # For Beanstalk SVN hooks
  robot.router.post '/hubot/beanstalk-svn', (req, res) ->
    console.log req
    data = JSON.parse req.body

    if data.message.indexOf("refs #") > -1
      haystack = /.*(#(\d+)).*/.exec commit.message
      issue_id = haystack[2]

      attributes =
        "notes": "#{data.author_full_name} changed #{data.changed_files.length} files:\n#{data.message}\n#{data.changeset_url}"

      redmine.Issue(issue_id).update attributes, (isserr, issdata, issstatus) ->


    res.writeHead 204, { 'Content-Length': 0 }
    res.end()

  # For Beanstalk git hooks
  robot.router.post '/hubot/beanstalk-commit', (req, res) ->
    console.log req
    data = JSON.parse req.body

    for commit in data.commits
      if commit.message.indexOf("refs #") > -1
        haystack = /.*(#(\d+)).*/.exec commit.message
        issue_id = haystack[2]

        attributes =
          "notes": "#{commit.author.name} changed #{commit.changed_files.length} files on #{data.branch} branch:\n#{commit.message}\n#{commit.url}"

        redmine.Issue(issue_id).update attributes, (isserr, issdata, issstatus) ->


    res.writeHead 204, { 'Content-Length': 0 }
    res.end()


formatIssueMessage = (data, url, msg) ->

  issue = data.issue

  #Object to push everything into
  fields = []

  #If it's a long description, truncate
  description = issue.description
  if description.length > 300
    description = "#{issue.description.substr(0, 300)}..."

  #Meat and potatoes
  fields.push
    title: "URL"
    value: url
    short: true
  fields.push
    title: "Project"
    value: issue.project.name
    short: true
  fields.push
    title: "Subject"
    value: issue.subject
    short: true
  fields.push
    title: "Priority"
    value: issue.priority.name
    short: true
  fields.push
    title: "Status"
    value: issue.status.name
    short: true
  fields.push
    title: "Currently Assigned To"
    value: issue.assigned_to.name
    short: true
  fields.push
    title: "Created"
    value: "#{issue.author.name}\n#{formatDate issue.created_on, 'mm/dd'}"
    short: true
  if issue.created_on isnt issue.updated_on
    fields.push
      title: "Updated"
      value: formatDate issue.updated_on, 'mm/dd at hh:ii ap'
      short: true


  # journals AKA ticket updates
  if issue.journals?
    #Heads up, history's coming
    fields.push
      title: "Recent History"
      value: "#{issue.journals.length} Updates"
      short: false

    #Limit journals to only so many; make an empty object to add to
    journalCount = 0

    #Redmine sorts entries ASC without sort in the issues/:id API
    issue.journals.reverse()

    #Loop through
    for journal in issue.journals
      do (journal) ->
        journalCount++

        #Only show first 3 journals
        if journalCount < 4

          #empty container
          field_value = ''

          #Journal description
          if journal.notes? and journal.notes != ""
            notes = journal.notes
            #Truncate notes
            if notes.length > 60
              notes = "#{notes.substr(0, 60).replace(/\n/g, '')}..."
            field_value += "\n\"#{notes}\" "

          #Display attachment count if relevant
          if attachmentCount > 0
            field_value += "\n_#{attachmentCount} attachments_"

          #Additional meta on the update
          if journal.details?

            #Count total attachments addded
            attachmentCount = 0

            for detail in journal.details

              #If there's a new attachemnt, increase the count
              if detail.property is "attachment"
                attachmentCount++

              #Reassigned
              if detail.name is "assigned_to_id"
                field_value += "\n_Reassigned_"

                #Would love to display pretty names here, but will involve promises
                #redmine.User(detail.new_value).show (usererr, userdata, userstatus) ->
                #  if userstatus is 200
                #    issueMessage.push "Assigned to #{userdata.user.firstname} #{userdata.user.lastname}"
                #  else
                #    issueMessage.push "Reassigned"

              #Status Chnage
              if detail.name is "status_id"
                status_target = parseInt detail.new_value
                if status_list[status_target]
                  field_value += "\n_Status changed to *#{status_list[status_target]}*_"
                else
                  field_value += "\n_Status changed_"

                #Method to dynamically get status pretty names
                #redmine.GetStatuses (statuserr, statusdata, statusesstatus) ->
                #  if statusesstatus is 200
                #    for issue_names in newdata.issue_statuses
                #      if issue_names.id is detail.new_value
                #        status_name = issue_names.name
                #        if status_name != ''
                #          issueMessage.push "Status changed to #{status_name}"
                #  else
                #    issueMessage.push "Status changed"

          fields.push
            title: "#{formatDate journal.created_on, 'mm/dd hh:ii ap'} (#{journal.user.name})"
            value: field_value
            short: true
        else if journalCount is 4
          #Exit loop if too many
          fields.push
            title: "But wait"
            value: "There's #{journalCount - 3} more!"
            short: true

          return false


  payload =
    message: msg.message
    content:
      text: issue.description
      fallback: "#{issue.project.name}\n#{issue.subject}\n#{issue.description}\n#{url}"
      pretext: ''
      color: "#E5283E" #the color of redmine
      mrkdwn_in: ["text", "title", "fallback", "fields"]
      fields: fields
  return payload

# simple ghetto fab date formatter this should definitely be replaced, but didn't want to
# introduce dependencies this early
#
# dateStamp - any string that can initialize a date
# fmt - format string that may use the following elements
#       mm - month
#       dd - day
#       yyyy - full year
#       hh - hours
#       ii - minutes
#       ss - seconds
#       ap - am / pm
#
# returns the formatted date
formatDate = (dateStamp, fmt = 'mm/dd/yyyy at hh:ii ap') ->
  d = new Date(dateStamp)
  # split up the date
  #hour offset - but this doesn't work with slack - (d.getTimezoneOffset() / 60)
  [m,d,y,h,i,s,ap] =
    [d.getMonth() + 1, d.getDate(), d.getFullYear(), d.getHours() - 7, d.getMinutes(), d.getSeconds(), 'AM']


  # leadig 0s
  i = "0#{i}" if i < 10
  s = "0#{s}" if s < 10

  # adjust hours
  if h > 12
    h = h - 12
    ap = "PM"

  # ghetto fab!
  fmt
    .replace(/mm/, m)
    .replace(/dd/, d)
    .replace(/yyyy/, y)
    .replace(/hh/, h)
    .replace(/ii/, i)
    .replace(/ss/, s)
    .replace(/ap/, ap)

# tries to resolve ambiguous users by matching login or firstname
# redmine's user search is pretty broad (using login/name/email/etc.) so
# we're trying to just pull it in a bit and get a single user
#
# name - this should be the name you're trying to match
# data - this is the array of users from redmine
#
# returns an array with a single user, or the original array if nothing matched
resolveUsers = (name, data) ->
    name = name.toLowerCase();

    # try matching login
    found = data.filter (user) -> user.login.toLowerCase() == name
    return found if found.length == 1

    # try first name
    found = data.filter (user) -> user.firstname.toLowerCase() == name
    return found if found.length == 1

    # give up
    data

# Redmine API Mapping
# This isn't 100% complete, but its the basics for what we would need in campfire
class Redmine
  constructor: (url, token) ->
    @url = url
    @token = token

  Users: (params, callback) ->
    @get "/users.json", params, callback

  User: (id) ->

    show: (callback) =>
      @get "/users/#{id}.json", {}, callback

  GetStatuses: (callback) ->
    @get "/issue_statuses.json", {}, callback

  Projects: (params, callback) ->
    @get "/projects.json", params, callback

  Issues: (params, callback) ->
    @get "/issues.json", params, callback

  Issue: (id) ->

    show: (params, callback) =>
      @get "/issues/#{id}.json", params, callback

    update: (attributes, callback) =>
      @put "/issues/#{id}.json", {issue: attributes}, callback

    add: (attributes, callback) =>
      @post "/issues.json", {issue: attributes}, callback

  TimeEntry: (id = null) ->

    create: (attributes, callback) =>
      @post "/time_entries.json", {time_entry: attributes}, callback

  # Private: do a GET request against the API
  get: (path, params, callback) ->
    path = "#{path}?#{QUERY.stringify params}" if params?
    @request "GET", path, null, callback

  # Private: do a POST request against the API
  post: (path, body, callback) ->
    @request "POST", path, body, callback

  # Private: do a PUT request against the API
  put: (path, body, callback) ->
    @request "PUT", path, body, callback

  # Private: Perform a request against the redmine REST API
  # from the campfire adapter :)
  request: (method, path, body, callback) ->
    headers =
      "Content-Type": "application/json"
      "X-Redmine-API-Key": @token

    endpoint = URL.parse(@url)
    pathname = endpoint.pathname.replace /^\/$/, ''

    options =
      "host"   : endpoint.hostname
      "port"   : endpoint.port
      "path"   : "#{pathname}#{path}"
      "method" : method
      "headers": headers

    if method in ["POST", "PUT"]
      if typeof(body) isnt "string"
        body = JSON.stringify body

      options.headers["Content-Length"] = body.length

    request = HTTP.request options, (response) ->
      data = ""

      response.on "data", (chunk) ->
        data += chunk

      response.on "end", ->
        switch response.statusCode
          when 200
            try
              callback null, JSON.parse(data), response.statusCode
            catch err
              callback null, (data or { }), response.statusCode
          when 401
            throw new Error "401: Authentication failed."
          else
            console.error "Code: #{response.statusCode}"
            callback null, null, response.statusCode

      response.on "error", (err) ->
        console.error "Redmine response error: #{err}"
        callback err, null, response.statusCode

    if method in ["POST", "PUT"]
      request.end(body, 'binary')
    else
      request.end()

    request.on "error", (err) ->
      console.error "Redmine request error: #{err}"
      callback err, null, 0
