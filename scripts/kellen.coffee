# Description:
#   Make a KELLEN
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot kellen - make a KELLEN
#
# Author:
#   tshedor based on code from brianstanwyck

kellen_array = [
  "http://i.imgur.com/9dWogDV.jpg",
  "http://i.imgur.com/bstaHCg.jpg",
  "http://i.imgur.com/jKKL1zk.jpg",
  "https://files.slack.com/files-pri/T02HBGDAR-F02HFC935/i-hate-you-dr-who.gif",
  "http://rainbowdash.net/file/xalltheysguy-20130411T233008-q3up3zn.gif",
  "PoundBangAlex",
  "Young Splendor",
  "STFU",
  "alex i hate you",
  "STOP YELLING AT ME NILES"
]

module.exports = (robot) ->
  robot.respond /kellen/i, (msg)->
    msg.send msg.random kellen_array