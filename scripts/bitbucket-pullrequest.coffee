# Description:
#   Holler when a pull request is made of accepted
#
# Dependencies:
#   None
#
# Configuration:
#   Set up a hook and get ready to rock
#   HUBOT_BITBUCKET_PULLREQUEST_ROOM
#
# Author:
#   tshedor

pr_room = process.env.HUBOT_BITBUCKET_PULLREQUEST_ROOM
green = '#48CE78'
blue = '#286EA6'
red = '#E5283E'

module.exports = (robot) ->
  robot.router.post '/hubot/bitbucket-pr/:repo', (req, res) ->
    repo_name = req.params.repo

    data = req.body
    msg = ''

    console.log data

    if data.hasOwnProperty('pullrequest_comment_created')
      resp = data.pullrequest_comment_created

      msg =
        message:
          reply_to: pr_room
          room: pr_room
        content:
          text: "New Comment"
          fallback: "#{resp.user.display_name} *added a comment* on `#{repo_name}`: \"#{resp.content.raw}\" \n\n#{resp.links.html.href}"
          pretext: ''
          color: blue
          mrkdwn_in: ["text", "title", "fallback", "fields"]
          fields: [
            {
              title: resp.user.display_name
              value: resp.content.raw
              short: true
            }
            {
              title: repo_name
              value: resp.links.html.href
              short: true
            }
          ]


    if data.hasOwnProperty('pullrequest_created')
      resp = data.pullrequest_created
      if resp.reviewers.length > 0
        reviewers = ''
        for reviewer in resp.reviewers
          reviewers += " #{reviewer.display_name}"
      else
        reviewers = 'To no one in particular'

      msg =
        message:
          reply_to: pr_room
          room: pr_room
        content:
          text: "New Request"
          fallback: "Yo#{reviewers}, #{resp.author.display_name} just *created* the pull request \"#{resp.title}\" for `#{resp.destination.branch.name}` on `#{repo_name}`."
          pretext: reviewers
          color: green
          mrkdwn_in: ["text", "title", "fallback", "fields"]
          fields: [
            {
              title: resp.author.display_name
              value: resp.title
              short: true
            }
            {
              title: repo_name
              value: "For #{resp.destination.branch.name}\n#{resp.links.html.href}"
              short: true
            }
          ]

    if data.hasOwnProperty('pullrequest_declined')
      resp = data.pullrequest_declined
      msg = branch_action(resp, 'Declined', red, repo_name)

    if data.hasOwnProperty('pullrequest_merged')
      resp = data.pullrequest_merged
      msg = branch_action(resp, 'Merged', green, repo_name)

    if data.hasOwnProperty('pullrequest_updated')
      resp = data.pullrequest_updated
      msg = branch_action(resp, 'Updated', blue, repo_name)

    if data.hasOwnProperty('pullrequest_approve')
      resp = data.pullrequest_approve
      encourage_array = [':thumbsup', 'That was a nice thing you did.', 'Boomtown', 'BOOM', 'Finally.', 'And another request bites the dust.']
      encourage_me = encourage_array[Math.floor(Math.random()*encourage_array.length)];

      msg =
        message:
          reply_to: pr_room
          room: pr_room
        content:
          text: "Pull Request Approved"
          fallback: "A pull request on #{repo_name} has been approved by #{resp.user.display_name}\n#{encourage_me}"
          pretext: encourage_me
          color: green
          mrkdwn_in: ["text", "title", "fallback", "fields"]
          fields: [
            {
              title: repo_name
              value: "Approved by #{resp.user.display_name}"
              short: false
            }
          ]

    if data.hasOwnProperty('pullrequest_unapprove')
      resp = data.pullrequest_unapprove
      msg =
        message:
          reply_to: pr_room
          room: pr_room
        content:
          text: "Pull Request Unapproved"
          fallback: "A pull request on #{repo_name} has been unapproved by #{resp.user.display_name}"
          pretext: 'Darn it.'
          color: red
          mrkdwn_in: ["text", "title", "fallback", "fields"]
          fields: [
            {
              title: repo_name
              value: "Unapproved by #{resp.user.display_name}"
              short: false
            }
          ]

    robot.emit 'slack-attachment', msg

    res.writeHead 204, { 'Content-Length': 0 }
    res.end()

branch_action = (resp, action_name, color, repo_name) ->
  fields = []
  fields.push
    title: resp.author.display_name
    value: resp.reason
    short: false
  fields.push
    title: "Repo"
    value: repo_name
    short: true
  fields.push
    title: "Branch"
    value: "From #{resp.source.branch.name}\nTo #{resp.destination.branch.name}"
    short: true

  payload =
    message:
      reply_to: pr_room
      room: pr_room
    content:
      text: "Pull Request \"#{resp.title}\" #{action_name}"
      fallback: "#{resp.author.display_name} *#{action_name}* pull request \"#{resp.title},\" to #{action_name} `#{resp.source.branch.name}` and `#{resp.destination.branch.name}` into a `#{repo_name}` super branch"
      pretext: ''
      color: color
      mrkdwn_in: ["text", "title", "fallback", "fields"]
      fields: fields

  return payload
